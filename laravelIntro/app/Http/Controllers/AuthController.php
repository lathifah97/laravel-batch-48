<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showForm(Request $request){
        return view ('form');
    }
    public function processSignUp(Request $request)
    {
        $firstname = $request->input('first-name');
        $lastname = $request->input('last-name');

        $request->session()->put('firstname', $firstname);
        $request->session()->put('lastname', $lastname);
        return redirect('/welcome');
    }
    public function showWelcome(Request $request){
        $firstname = $request->session()->get('firstname');
        $lastname = $request->session()->get('lastname');
        return view ('welcome', compact('firstname', 'lastname'));
    }
}
