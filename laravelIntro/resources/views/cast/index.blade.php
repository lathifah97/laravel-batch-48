@extends('layout.master')

@section('judul')
Cast
@endsection

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List of Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      
      <a href="/cast/create" class="btn btn-primary">Tambah</a>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        
          @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td></td>
                        <td>No data</td>
                        <td></td>
                    </tr>  
                @endforelse        
        </tbody>
        <tfoot>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('script')
    <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(document).ready(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush