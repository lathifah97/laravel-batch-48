@extends('layout.master')

@section('judul')
Menampilkan Cast
@endsection

@section('content')

<h2>Tampilkan Cast {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

@endsection