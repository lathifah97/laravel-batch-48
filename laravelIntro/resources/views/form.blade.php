@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
<h2>Sign Up Form</h2>
    <form method="post" action="/sign-up">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first-name" required><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last-name" required><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="Gender" checked>Male<br>
        <input type="radio" name="Gender">Female<br>
        <input type="radio" name="Gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="Country"><br>
            <option value="Brunei">Brunei</option>
            <option value="Filipina">Filipina</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Laos">Laos</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Thailand">Thailand</option>
            <option value="Timor Leste">Timor Leste</option>
            <option value="Vietnam">Vietnam</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Bahasa Indonesia" checked>Bahasa Indonesia<br>
        <input type="checkbox" name="Other">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="Bio" rows="1" cols="30"></textarea>
        <br><br>
        <input type="Submit" value="Sign up">
    </form>
@endsection
{{-- <!DOCTYPE html>
<html>
<head>
    <title>SanberBook - Formulir Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="post" action="/sign-up">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first-name" required><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last-name" required><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="Gender" checked>Male<br>
        <input type="radio" name="Gender">Female<br>
        <input type="radio" name="Gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="Country"><br>
            <option value="Brunei">Brunei</option>
            <option value="Filipina">Filipina</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Laos">Laos</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Thailand">Thailand</option>
            <option value="Timor Leste">Timor Leste</option>
            <option value="Vietnam">Vietnam</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Bahasa Indonesia" checked>Bahasa Indonesia<br>
        <input type="checkbox" name="Other">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="Bio" rows="1" cols="30"></textarea>
        <br><br>
        <input type="Submit" value="Sign up">
    </form>
</body>
</html> --}}