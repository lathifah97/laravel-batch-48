<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>OOP</title>
</head>
<body>
<h1>OOP PHP</h1>
<?php
include("animal.php");
$sheep = new Animal("Shaun");
echo "Nama: " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold-blooded : " . $sheep->cold_blooded . "<br><br>"; 

include("Ape.php");
$ape = new Ape("KeraSakti");
echo "Nama: " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold-blooded : " . $ape->cold_blooded . "<br>"; 
$ape->yell();
echo "<br>";

include("Frog.php");
$frog = new Frog("Buduk");
echo "Nama: " . $frog->name . "<br>";
echo "Legs: " . $frog->legs . "<br>";
echo "Cold-blooded : " . $frog->cold_blooded . "<br>";
$frog->jump();

?>
</body>
</html>