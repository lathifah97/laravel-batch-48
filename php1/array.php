<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>
    
    <?php
    echo "<h3> Soal 1 </h3>";
    $kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven");
    $adults = array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray");
    echo "<pre>";
    echo "Kids : "; print_r($kids); echo "<br>";
    echo "Adult : "; print_r($adults);    
    echo "</pre>";

    echo "<h3> Soal 2</h3>";
    $panjang_kid=count($kids);
    echo "Cast Stranger Things ";
    echo "<br>";
    echo "<br>";
    echo "Total Kids : $panjang_kid";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    echo "</ol>";
    $panjang_adult=count($adults);
    echo "Total Adults : $panjang_adult"; 
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    echo "</ol>";

    echo "<h3> Soal 3</h3>";
    $arr = [
            ["Name"=> "Will Byers",
            "Age"=> 12,
            "Aliases"=> "Will the Wise",
            "Status"=> "Alive"],

            ["Name"=> "Mike Wheeler",
            "Age"=> 12,
            "Aliases"=> "Dungeon Master",
            "Status"=> "Alive"],

            ["Name"=> "Jim Hopper",
            "Age"=> 43,
            "Aliases"=> "Chief Hopper",
            "Status"=> "Deceased"],

            ["Name"=> "Eleven",
            "Age"=> 12,
            "Aliases"=> "El",
            "Status"=> "Alive"]
    ];
    echo "<pre>";
    print_r( $arr);
    echo "</pre>";
    ?>
</body>

</html>